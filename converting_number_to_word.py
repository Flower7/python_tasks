from num2words import num2words
from Python import do_continue


def number_to_word(number):
    return num2words(number, lang='ru')


if __name__ == '__main__':
    is_continue = True
    while is_continue:
        try:
            n = input('Enter a number: ')
            print(n, '==>', number_to_word(n))
        except ValueError:
            print('Invalid enter.\n')
        response = str(input('Do you want to continue? [y / yes]\n'))
        is_continue = do_continue(response)
    print('The program is completed.')

