"""
Task "Envelopes analysis": to check if one envelope can be put into another one.
If the user enters non-numeric value he gets the message “Invalid value. Please try again.“.
After the comparison of the envelopes user gets a message “Do you want to continue? [y/yes]“
The program will end its work or will continue depending on the user's response.
In the end user gets a message “The program is completed.“. It means that the program has completed its work.
"""


from Python.do_continue import do_continue


def comparison_of_envelopes(first_envelope_first_side, first_envelope_second_side,
                            second_envelope_first_side, second_envelope_second_side):
    if (first_envelope_first_side ** 2 + first_envelope_second_side ** 2 <
            second_envelope_first_side ** 2 + second_envelope_second_side ** 2):
        return 'You can put the first envelope into the second envelope.\n'
    if (first_envelope_first_side ** 2 + first_envelope_second_side ** 2 >
            second_envelope_first_side ** 2 + second_envelope_second_side ** 2):
        return 'You can put the second envelope into the first envelope.\n'
    return 'You cannot put one envelope into another one.\n'


def validation(side):
    try:
        entered_side = float(side)
        if entered_side > 0:
            return entered_side
        else:
            raise ValueError
    except ValueError:
        print('Invalid value. Please try again.\n')
        return 0


if __name__ == '__main__':
    is_continue = True
    while is_continue:
        print('Please enter the sizes of the envelopes. Values must be greater than 0.')
        try:
            side = input('Enter height of the first envelope: ')
            side_a = validation(side)
            side = input('Enter width of the first envelope: ')
            side_b = validation(side)
            side = input('Enter height of the second envelope: ')
            side_c = validation(side)
            side = input('Enter width of the second envelope: ')
            side_d = validation(side)
        except ValueError:
            print('Invalid value. Values must be greater than 0.\n')
            continue
        print(comparison_of_envelopes(side_a, side_b, side_c, side_d))
        response = str(input('Do you want to continue? [y / yes]\n'))
        is_continue = do_continue(response)
    print('The program is completed.')
