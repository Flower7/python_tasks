"""
Envelopes analysis:
to check if one envelope can be put into another one.
If the user enters non-numeric value he gets the message “Invalid value. Please try again.“.
After the comparison of the envelopes user gets a message “Do you want to continue? [y/yes]“
The program will end its work or will continue depending on the user's response.
In the end user gets a message “The program is completed.“. It means that the program has completed its work.
"""


from abc import ABC, abstractmethod
from typing import Any


class Input(ABC):

    @abstractmethod
    def input_side(self) -> Any:
        pass


class FloatInput(Input):
    def __init__(self, value: str):
        self._value = value

    def input_side(self) -> float:
        while True:
            try:
                entered_side = float(input('Enter {0} of the envelope: '.format(self._value)))
                return entered_side
            except ValueError:
                print('Invalid value. Please try again.\n')


class Rule(ABC):

    @abstractmethod
    def passed(self, value: Any) -> bool:
        pass


class GreaterThenZero(Rule):
    def passed(self, value: float) -> bool:
        return value > 0


class CheckingRules(Rule):
    def __init__(self, *rules: Any):
        self._test = rules

    def passed(self, value: float) -> bool:
        passed = True
        for rule in self._test:
            if not rule.passed(value):
                print(f'{rule.__class__.__name__} is failed. Please try again.\n')
                passed = True
        return passed


class Envelope:
    def __init__(self, first_side: float, second_side: float):
        self._first_side = first_side
        self._second_side = second_side
        self._diagonal = first_side ** 2 + second_side ** 2

    def __lt__(self, other) -> bool:
        return self._diagonal < other._diagonal


class Choice:
    def do_continue(self) -> bool:
        response = input('Do you want to continue? [y / yes]\n')
        if (response.lower() == 'y') or (response.lower() == 'yes'):
            return True
        else:
            return False


if __name__ == '__main__':
    print('Please enter the sizes of the envelopes. Values must be greater than 0.')
    is_continue = True
    while is_continue:
        height = FloatInput("height")
        width = FloatInput("width")
        tested_side = CheckingRules(GreaterThenZero())
        first_envelope = Envelope(tested_side.passed(height.input_side()), tested_side.passed(width.input_side()))
        second_envelope = Envelope(tested_side.passed(height.input_side()), tested_side.passed(width.input_side()))
        print(f'You can put the first envelope into the second envelope. ==> {first_envelope < second_envelope}\n'
              f'You can put the second envelope into the first envelope. ==> {first_envelope > second_envelope}')
        is_continue = Choice().do_continue()
    print('The program is completed.')
